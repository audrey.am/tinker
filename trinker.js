const { america } = require("colors");

module.exports = {
  title: function () {
    return `
$$$$$$$$\\        $$\\           $$\\                           $$\\                               
\\__$$  __|       \\__|          $$ |                          $$ |                              
   $$ | $$$$$$\\  $$\\ $$$$$$$\\  $$ |  $$\\  $$$$$$\\   $$$$$$\\  $$ | $$$$$$\\ $$\\    $$\\  $$$$$$\\  
   $$ |$$  __$$\\ $$ |$$  __$$\\ $$ | $$  |$$  __$$\\ $$  __$$\\ $$ |$$  __$$\\\\$$\\  $$  |$$  __$$\\ 
   $$ |$$ |  \\__|$$ |$$ |  $$ |$$$$$$  / $$$$$$$$ |$$ |  \\__|$$ |$$ /  $$ |\\$$\\$$  / $$$$$$$$ |
   $$ |$$ |      $$ |$$ |  $$ |$$  _$$<  $$   ____|$$ |      $$ |$$ |  $$ | \\$$$  /  $$   ____|
   $$ |$$ |      $$ |$$ |  $$ |$$ | \\$$\\ \\$$$$$$$\\ $$ |$$\\   $$ |\\$$$$$$  |  \\$  /   \\$$$$$$$\\ 
   \\__|\\__|      \\__|\\__|  \\__|\\__|  \\__| \\_______|\\__|\\__|  \\__| \\______/    \\_/     \\_______|
================================================================================================
   `.yellow;
  },

  line: function (title = "=") {
    return title.padStart(48, "=").padEnd(96, "=").bgBrightYellow.black;
  },

  allMale: function (p) {
    let t = [];
    for (x of p) {
      if (x.gender === "Male") {
        t.push(x);
      }
    }
    return t;
  },

  allFemale: function (p) {
    let t = [];
    for (x of p) {
      if (x.gender === "Female") {
        t.push(x);
      }
    }
    return t;
  },
  //LEVEL1

  nbOfMale: function (p) {
    return this.allMale(p).length;
  },

  nbOfFemale: function (p) {
    return this.allFemale(p).length;
  },

  nbOfMaleInterest: function (p) {
    let n = 0;
    for (x of p) {
      if (x.looking_for === "M") {
        n++;
      }
    }
    return n;
  },

  nbOfFemaleInterest: function (p) {
    let n = 0;
    for (x of p) {
      if (x.looking_for === "F") {
        n++;
      }
    }
    return n;
  },

  nbOfRich: function (p) {
    let n = [];
    for (x of p) {
      y = x.income.substring(1);
      if (parseFloat(y) > 2000) {
        n.push(x);
      }
    }
    return n.length;
  },

  nbLikeDrama: function (p) {
    let n = [];
    for (x of p) {
      if (x.pref_movie.includes("Drama")) {
        n.push(x);
      }
    }
    return n.length;
  },

  nbFLikeSF: function (p) {
    let n = [];
    for (x of p) {
      if (x.gender === "Female" && x.pref_movie.includes("Sci-Fi")) {
        n.push(x);
      } //include
    }
    return n.length;
  },

  //LEVEL2

  nbLikesDocAndIncomeMedium: function (p) {
    let n = [];
    for (x of p) {
      y = x.income.substring(1);
      if (x.pref_movie.includes("Documentary") && parseFloat(y) > 1482) {
        n.push(x);
      }
    }
    return n.length;
  },

  listKingsOfMoula: function (p) {
    let n = [];
    for (x of p) {
      y = x.income.substring(1);
      if (y > 4000) {
        let k = [x.last_name, x.first_name, x.id, x.income];
        n.push(k);
      }
    }
    return n;
  },

  hommeLePlusRiche: function (p) {
    let tous_les_hommes = this.allMale(p);
    let le_plus_riche;
    let salaire_du_plus_riche = 0;
    for (let x of tous_les_hommes) {
      if (tune(x) > salaire_du_plus_riche) {
        le_plus_riche = x;
        salaire_du_plus_riche = tune(x);
      }
    }
    return le_plus_riche.last_name + " " + le_plus_riche.id;
  },

  salaireMoyen: function (p) {
    let salaireAjoutes = 0.0;
    for (personne of p) {
      salaireAjoutes = salaireAjoutes + tune(personne);
    }
    return salaireAjoutes / p.length;
  },

  salaireMediane: function (p) {
    let tableautune = [];
    for (personne of p) {
      tableautune.push(tune(personne));
    }
    tableautune.sort((a, b) => a - b);

    let middle = tableautune.length / 2;

    let left = tableautune[middle + 1];
    let right = tableautune[middle];
    return (left + right) / 2;
  },

  personneNord: function (p) {
    let n = [];
    for (x of p) {
      if (x.latitude > 0.0) {
        n.push(x);
      }
    }
    return n.length;
  },

  salaireMoyenHemSud: function (p) {
    return this.salaireMoyen(personneSud(p));
  },

  //LEVEL3

  pythagore: function (a, b) {
    return Math.sqrt(Math.pow(b.long - a.long, 2) + Math.pow(b.lat - a.lat, 2));
  },

  PersonneProcheDeBerenice61: function (people) {
    const found = people.find((element) => element.last_name == "Cawt");
    let a = { long: 0, lat: 0 };
    a.long = found.longitude;
    a.lat = found.latitude;

    let gap = 999999999999.0;
    let nearest_User = [];
    for (let i of people) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }

    return nearest_User;
  },

  //   let distance = 0.0;
  //   let gap = Number.MAX_VALUE;
  //   let gens_le_plus_proche;
  //   const found = people.find((element) => element.last_name == "Cawt");
  //   const latitudeqq1 = found.latitude;
  //   const longitudeqq1 = found.longitude;

  //   for (let un_gens of people) {
  //     let a = un_gens.latitude - latitudeqq1;
  //     let b = un_gens.longitude - longitudeqq1;
  //     distance = Math.sqrt(
  //       Math.pow(longitudeqq1 - b, 2) + Math.pow(latitudeqq1 - a, 2)
  //     );

  //     if (distance < gap && distance !== 0) {
  //       gap = distance;
  //       gens_le_plus_proche = un_gens;
  //     }
  //   }

  //   return gens_le_plus_proche.id + " " + gens_le_plus_proche.last_name;
  // },

  PersonneProcheDeRui: function (people) {
    const found = people.find((element) => element.last_name == "Brach");
    let a = { long: 0, lat: 0 };
    a.long = found.longitude;
    a.lat = found.latitude;

    let gap = 999999999999.0;
    let nearest_User = [];
    for (let i of people) {
      let b = { long: i.longitude, lat: i.latitude };
      if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
        gap = this.pythagore(a, b);
        nearest_User = [i.last_name, i.id];
      }
    }

    return nearest_User;
  },
  // let distance = 0.0;
  // let distance_la_plus_proche = 0.0;
  // let gens_le_plus_proche
  // const found = people.find(element => element.last_name == "Brach");
  // const latitudeqq1 = found.latitude;
  // const longitudeqq1 = found.longitude;
  // let gap = Number.MAX_VALUE;
  // for(let un_gens of people){
  //     let a = un_gens.latitude - latitudeqq1
  //     let b = un_gens.longitude - longitudeqq1
  //     distance = Math.sqrt(Math.pow(longitudeqq1 - b, 2) + Math.pow(latitudeqq1 - a, 2))

  //     if(distance < gap && distance !== 0 ){
  //         gap = distance
  //         gens_le_plus_proche = un_gens
  //     }
  // }

  // return gens_le_plus_proche.id + " " + gens_le_plus_proche.last_name
  // },

  PersonneProcheDeJoseeBoshard: function (people) {
    const found = people.find((element) => element.last_name == "Boshard");
    let a = { long: 0, lat: 0 };
    a.long = found.longitude;
    a.lat = found.latitude;

    let tableau = [];

    let nearest_User = [];

    for (let i of people) {
      let b = { long: i.longitude, lat: i.latitude };

      if (this.pythagore(a, b) !== 0) {
        nearest_User = [this.pythagore(a, b)];
        tableau.push(nearest_User);
      }
    }
    tableau.sort((a, b) => a - b);
    tableau.length = 10;
    let tableau2 = [];

    for (let i of tableau) {
      for (let x of people) {
        let b = { long: x.longitude, lat: x.latitude };

        if (this.pythagore(a, b) !== 0 && this.pythagore(a, b) == i) {
          tableau2.push({ nom: x.last_name, id: x.id });
        }
      }
    }
    return tableau2;
  },

  googleTeam: function (p) {
    let n = [];
    for (x of p) {
      if (x.email.includes("google")) {
        n.push(x.id, x.last_name);
      }
    }
    return n;
  },

  ageVieux: function (people) {
    let date_naissance_plus_vieux = new Date();
    let le_plus_vieux;
    for (let un_gens of people) {
      if (date_anniv_modif(un_gens) < date_naissance_plus_vieux) {
        date_naissance_plus_vieux = date_anniv_modif(un_gens);
        le_plus_vieux = un_gens;
      }
    }

    return le_plus_vieux.last_name + " " + le_plus_vieux.first_name;
  },

  agejeune: function (people) {
    let date_naissance_plus_jeune = new Date(1900, 01, 01);
    let le_plus_jeune;
    for (let un_gens of people) {
      if (date_anniv_modif(un_gens) > date_naissance_plus_jeune) {
        date_naissance_plus_jeune = date_anniv_modif(un_gens);
        le_plus_jeune = un_gens;
      }
    }

    return le_plus_jeune.last_name + " " + le_plus_jeune.first_name;
  },

  diff_age: function (p) {
    let tableau = [];
    let tableaucomp = [];
    let aujourdhui = new Date();

    for (un_gens of p) {
      let age =
        aujourdhui.getFullYear() - date_anniv_modif(un_gens).getFullYear();
      tableau.push(un_gens.age);
      {
        for (un_deuxieme_gens of p) {
          let age2 =
            aujourdhui.getFullYear() -
            date_anniv_modif(un_deuxieme_gens).getFullYear();
          tableau.push(un_deuxieme_gens.age);

          if (age > age2) {
            let comp = age - age2;
            tableaucomp.push(comp);
          } else {
            tableaucomp.push(age2 - age);
          }
        }
      }
    }

    const reduire = (accumulator, curr) => accumulator + curr;
    return tableaucomp.reduce(reduire) / (tableaucomp.length - 1000);
  },
  //je pense qu'il se compare lui meme(-1000?)

  //LEVEL4
  genre_le_plus_pop: function (people) {
    let le_plus_populaire = this.genre_popularite(people);
    let compte = 0;
    for (x in le_plus_populaire) {
      if (x[1] > compte) {
        compte = x[1];
      }
      return x;
    }
  },

  genre_popularite: function (people) {
    let populaire = this.liste_de_genre_et_Nb_de_personne(people);
    for (x in populaire) {
      populaire.sort((a, b) => a - b);
    }
    return populaire;
  },

  liste_de_genre_et_Nb_de_personne: function (people) {
    let liste = [];
    for (personne of people) {
      var splitted = personne.pref_movie.split("|");
      liste.push(splitted);
    }
    const array1 = liste.reduce(function (a, b) {
      return a.concat(b);
    });

    var count = [];
    for (var i = 0, j = array1.length; i < j; i++) {
      count[array1[i]] = (count[array1[i]] || 0) + 1;
    }
    return count;
  },

  HfilmsNoirs: function (p) {
    let tous_les_hommes = this.allMale(p);
    let ajout_age = 0;
    let aujourdhui = new Date();
    let tableau = [];
    for (homme of tous_les_hommes) {
      if (homme.pref_movie.includes("Film-Noir")) {
        let age =
          aujourdhui.getFullYear() - date_anniv_modif(homme).getFullYear();
        tableau.push(age);

        ajout_age = ajout_age + age;
      }
    }
    return ajout_age / tableau.length;
  },
  // pas pareil que les autres... peut etre à cause du "get.fullYear" dans ma moy_age(36,875)(personne né le 06.12)

  FilmNoirAParisetPatriarcat: function (p) {
    let toutes_les_femmes = this.allFemale(p);
    let tous_les_hommes = this.allMale(p);
    let ajout_age = 0;
    let aujourdhui = new Date();
    let tableau = [];
    for (femme of toutes_les_femmes) {
      if (
        femme.pref_movie.includes("Film-Noir") &&
        femme.longitude > -7.5 &&
        femme.longitude < 12.5 &&
        tune(femme) < this.salaireMoyen(tous_les_hommes)
      ) {
        let age =
          aujourdhui.getFullYear() - date_anniv_modif(femme).getFullYear();
        tableau.push(age);

        ajout_age = ajout_age + age;
      }
    }
    return ajout_age / tableau.length;
  },

  HchercheH: function (p) {
    let tous_les_hommes = this.allMale(p);
    let interet_pour_les_hommes = MaleInterest(p);
    let a = { long: 0, lat: 0 };
    let b = { long: 0, lat: 0 };
    let gap = 999999999999.0;
    let nearest_User = [];
    let tableau1 = [];
    let tableau2 = [];
    for (let garcon1 of tous_les_hommes) {
      for (let aime_garcon1 of interet_pour_les_hommes) {
        if (garcon1 == aime_garcon1) {
          tableau1.push(garcon1);
        }
      }

      for (let garcon2 of tous_les_hommes) {
        for (let aime_garcon2 of interet_pour_les_hommes) {
          if (garcon2 == aime_garcon2) {
            tableau2.push(garcon2);
          }
        }
      }
    }
    for (personne1 of tableau1) {
      for (personne2 of tableau2) {
        if (
          personne1.pref_movie.includes("") ==
            personne2.pref_movie.includes("") &&
          personne1 != personne2
        ) {
          a = { long: personne1.longitude, lat: personne1.latitude };

          {
            b = { long: personne2.longitude, lat: personne2.latitude };
            if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
              gap = this.pythagore(a, b);
            }
            nearest_User = [
              personne1.last_name,
              personne1.first_name,
              personne2.last_name,
              personne2.first_name,
              gap,
            ];
          }
        }
      }
    }
    return nearest_User;
  },

  Liste_des_couples_meme_pref_film: function (p) {
    let tous_les_hommes = this.allMale(p);
    let toutes_les_femmes = this.allFemale(p);
    let interet_pour_les_femmes = FemaleInterest(p);
    let interet_pour_les_hommes = MaleInterest(p);
    let match = [];
    let tableauH = [];
    let tableauF = [];
    let tableau = [];
    for (let homme of tous_les_hommes) {
      for (let aime_femme of interet_pour_les_femmes) {
        if (homme == aime_femme) {
          tableauH.push(homme);
        }
      }

      for (let femme of toutes_les_femmes) {
        for (let aime_homme of interet_pour_les_hommes) {
          if (femme == aime_homme) {
            tableauF.push(femme);
          }
        }
      }
    }

    for (garcon of tableauH) {
      for (fille of tableauF) {
        if (garcon.pref_movie.includes("") === fille.pref_movie.includes("")) {
          match = [
            fille.id,
            fille.last_name,
            fille.first_name,
            garcon.id,
            garcon.last_name,
            garcon.first_name,
          ];
          tableau.push(match);
        }
      }
    }

    return tableau;
  },

  match: function (p) {
    let tous_les_hommes = this.allMale(p);
    let toutes_les_femmes = this.allFemale(p);
    let interet_pour_les_femmes = FemaleInterest(p);
    let interet_pour_les_hommes = MaleInterest(p);
    let a = { long: 0, lat: 0 };
    let b = { long: 0, lat: 0 };
    let gap = 999999999999.0;
    let nearest_User = [];
    let tableau1 = [];
    let tableau2 = [];
    let tableauH = [];
    let tableauF = [];

    for (let homme of tous_les_hommes) {
      for (let aime_femme of interet_pour_les_femmes) {
        if (homme == aime_femme) {
          tableauH.push(homme);
        }
      }

      for (let femme of toutes_les_femmes) {
        for (let aime_homme of interet_pour_les_hommes) {
          if (femme == aime_homme) {
            tableauF.push(femme);
          }
        }
      }
    } //couple hétéro (checked)

    for (let personne1 of tableauH) {
      for (let personne2 of tableauF) {
        if (
          personne1.pref_movie.includes("") ==
            personne2.pref_movie.includes("") &&
          personne1 != personne2
        ) {
          // if (
          //   personne2.tune(p) <
          //       personne1.tune(p) - personne1.tune(p) * 0.1 &&
          //         personne2.tune(p) > personne1.tune(p) / 2)
          //  {
            //10% de moins et plus que la moitié pour les femmes, 
            //personne2.tune n'est pas une fonction??

            a = { long: personne1.longitude, lat: personne1.latitude };

            {
              b = { long: personne2.longitude, lat: personne2.latitude };
              if (this.pythagore(a, b) < gap && this.pythagore(a, b) !== 0) {
                gap = this.pythagore(a, b);
              }
              nearest_User = [
                personne1.last_name,
                personne1.first_name,
                personne2.last_name,
                personne2.first_name,
                gap,
              ];
            }
          }
        }
      // }
    }
    return nearest_User;
    //geoloc et pref film

    // for (let garcon1 of tous_les_hommes) {
    //   for (let aime_garcon1 of interet_pour_les_hommes) {
    //     if (garcon1 == aime_garcon1) {
    //       tableau1.push(garcon1);
    //     }
    //   }

    //   for (let garcon2 of tous_les_hommes) {
    //     for (let aime_garcon2 of interet_pour_les_hommes) {
    //       if (garcon2 == aime_garcon2) {
    //         tableau2.push(garcon2);
    //       }
    //     }
    //   }
    // } //couple homo hommes

    // for (let fille1 of toutes_les_femmes) {
    //   for (let aime_fille1 of interet_pour_les_femmes) {
    //     if (fille1 == aime_fille1) {
    //       tableau3.push(fille1);
    //     }
    //   }

    //   for (let fille2 of toutes_les_femmes) {
    //     for (let aime_fille2 of interet_pour_les_femmes) {
    //       if (fille2 == aime_fille2) {
    //         tableau4.push(fille2);
    //       }
    //     }
    //   }
    // } //couple homo femmes

    //  for (personne in p) {
    //     if (looking_for == "M")homme.tune(p) - (homme.tune(p)* 0.1)rer les gens les plus proche(voir Berenice)
    // gout en commun (liste des genres if male.film_pref == personne.film_pref)
    // if personne.genre = Female , verifier que 10%de salaire en moins (patriarcat)
    // if personne.mail.includes 'google' (match avec google... comment ?)
    // if personne.film_pref.includes('drama') (pareil que google))
    // if personne.film_pref.includes(' aventure')(pareil que google)

    //     }else{
    //    pareil sauf patriarcat
    //     }
    //  }
  },
};

function tune(p) {
  return parseFloat(p.income.substring(1));
}

function personneSud(p) {
  let n = [];
  for (x of p) {
    if (x.latitude < 0.0) {
      n.push(x);
    }
  }
  return n;
}
function date_anniv_modif(p) {
  return new Date(p.date_of_birth);
}
// function moy_age(p) {
//   let ajout_age = 0;
//   let aujourdhui = new Date();
//   let tableau = [];
//   for (un_gens of p) {
//     let age =
//       aujourdhui.getFullYear() - date_anniv_modif(un_gens).getFullYear();
//     tableau.push(age);
//     ajout_age = ajout_age + age;
//   }
//   return ajout_age / tableau.length;
// }
function MaleInterest(p) {
  let n = [];
  for (m of p) {
    if (m.looking_for === "M") {
      n.push(m);
    }
  }
  return n;
}
function FemaleInterest(p) {
  let n = [];
  for (f of p) {
    if (f.looking_for === "F") {
      n.push(f);
    }
  }
  return n;
}
// function Patriarcat(p) {
//   let tous_les_hommes = allMale(p);
//   for (homme of allMale){
//     return homme.tune(p) - (homme.tune(p)* 0.1)
//   }

// }
